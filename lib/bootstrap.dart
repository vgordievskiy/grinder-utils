library grinder_utils.bootstrap;
import 'dart:io';
import 'package:colorize/colorize.dart';
import 'package:logging/logging.dart';

void bootstrapGrind() {
  bootstrapLogger();
}

void bootstrapLogger() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    StringBuffer message = new StringBuffer();
    message.write('${rec.level.name}:${rec.loggerName} ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      message.write(' ${rec.error}');
    }

    if (rec.stackTrace != null) {
      message.write(' ${rec.stackTrace}');
    }

    String out = message.toString();
    if(rec.level == Level.WARNING)
      stdout.write(new Colorize(out)..yellow());
    else if(rec.level > Level.WARNING)
      stderr.write(new Colorize(out)..red());
    else
      stdout.write(out);
  });

}
