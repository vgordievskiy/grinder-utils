library grinder_utils;

import 'dart:io';
import 'package:grinder/grinder.dart';
import 'package:grinder_utils/utils/svg/svg_sprite_config.dart';
import 'package:grinder_utils/utils/svg_fix_translate/svg_fixer.dart';
import 'package:path/path.dart' as path;
import 'package:xml/xml.dart' as xml;
import 'package:yaml/yaml.dart' as yaml_lib;
import 'package:grinder_utils/config/app_config.dart';
import 'package:grinder_utils/utils/svg/svg_sprite.dart';
import 'package:grinder_utils/utils/svg_fix_translate/svg_fix_config.dart';

export 'package:grinder/grinder.dart';

final AppConfig appConfig = new AppConfig();

@Task('combine all svg')
combine_svg() {
  SvgSpriteConfig config = SvgSpriteConfig.read();

  if (config != null ) {
    if (config.file != null && config.paths != null) {
      SvgSpriteResult svgData = new SvgSprite().combineSvgs(config.paths, config.excludePaths??[], [config.filePath]);

      File svgFile = new File(config.filePath);
      svgFile.createSync(recursive: true);
      svgFile.writeAsStringSync(svgData.svgSprite);
    }
  }
}

@Task('fix all svg')
fix_svg() {
  SvgFixConfig config = SvgFixConfig.read();

  if (config != null ) {
    if (config.outDir != null && config.files != null) {
      SvgFixResult svgData = new SvgFixer().fixSvgFiles(config.files, 'translate');

      svgData.files.forEach((String name, xml.XmlNode node){
        File svgFile = new File(config.outDir + "/${name}");
        svgFile.createSync(recursive: true);
        svgFile.writeAsStringSync(node.toXmlString());
      });
    }
  }
}
