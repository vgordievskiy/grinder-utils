library grinder_utils.svg.fix.config;
import 'dart:io';
import 'package:yaml/yaml.dart' as yaml_lib;

class SvgFixConfig {
  String outDir;
  List<String> files;

  static SvgFixConfig read() {
    File f = new File("pubspec.yaml");
    String content = f.readAsStringSync();
    Map yaml = yaml_lib.loadYaml(content);
    var yamlCfg = (yaml['grinder_utils']);

    if (yamlCfg != null ) {
      var cfg = yamlCfg['svg_translate_fix'];

      if (cfg != null && cfg['files'] != null && cfg['outDir'] != null) {
        return new SvgFixConfig()
          ..outDir = cfg['outDir']
          ..files = cfg['files'] as List<String>;
      }
    }
    return null;
  }
}
