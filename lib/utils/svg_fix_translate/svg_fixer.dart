library grinder_utils.svg.fixer;

import 'dart:io';
import 'package:xml/xml.dart';

class SvgFixResult {
  Map<String, XmlNode> files = {};
}

typedef XmlNode TFixer(XmlNode node);

class SvgFixer {
  XmlNode _getSvgNode(String svgText) {
    XmlDocument svgXml = parse(svgText);
    return svgXml.findElements("svg").first;
  }

  final Map<String, TFixer> fixers = {
    'translate' : translateFix
  };

  SvgFixResult fixSvgFiles(List<String> files, String type) {
    SvgFixResult result = new SvgFixResult();

    for (String file in files) {
      File item = new File(file);
      if (!item.existsSync()) {
        continue;
      }
      XmlNode xmlOrigin = _getSvgNode(item.readAsStringSync());
      result.files[item.uri.pathSegments.last] = fixers[type](xmlOrigin);
    }

    return result;
  }

  static _addAttributes(XmlNode node, XmlBuilder  builder,
    { List<String> filterAttrubutes: const [], List<String> filterAttrubutesValues: const []})
  {
    for (XmlAttribute attr in node.attributes) {
      String attrName = attr.name.toString();
      if (!filterAttrubutes.any((String attr) => attrName.toLowerCase() == attr)
          && !filterAttrubutesValues.any((String val) => attr.value.contains(val))
         )
      {
        builder.attribute(attr.name.toString(), attr.value);
      }
    }
  }

  static XmlNode translateFix(XmlNode node) {
    XmlBuilder builder = new XmlBuilder();
    int level = 0;
    _addNode(XmlNode parent, XmlBuilder  builder,
      {List<String> filterAttrubutes: const [], List<String> filterAttrubutesValues: const []})
    {
      ++level;
      for (XmlNode node in parent.children) {
        if (node.nodeType == XmlNodeType.ELEMENT) {
          XmlNamed namedElement = node as XmlNamed;
          builder.element(namedElement.name.toString(), nest: () {
            _addAttributes(node, builder,
              filterAttrubutes: filterAttrubutes,
              filterAttrubutesValues: filterAttrubutesValues);
            _addNode(node, builder);
          });
        }  else if (node.nodeType == XmlNodeType.TEXT && !node.text.trim().isEmpty) {
          builder.text(node.text.trim());
        }
      }
      --level;
    }

    builder.element('svg', nest: () {
      builder.attribute('xmlns', 'http://www.w3.org/2000/svg');
      builder.attribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
      builder.element('defs', nest: () {
        node.children.where((XmlNode el) {
          if (el.nodeType == XmlNodeType.ELEMENT) {
            final XmlNamed named = el as XmlNamed;
            final String name = named.name.toString();
            if (name == 'g' || name == 'use' || name == "defs") {
              return true;
            }
          }
          return false;
        }).forEach((XmlNode el){
          XmlNamed namedElement = el as XmlNamed;
          if (namedElement.name.toString() != 'defs') {
            builder.element(namedElement.name.toString(), nest: () {
              var filter = [ 'translate'];
              _addAttributes(el, builder, filterAttrubutesValues: filter);
              _addNode(el, builder, filterAttrubutesValues: filter);
            });
          } else {
            for (XmlNode path in el.children) {
              if (path.nodeType == XmlNodeType.ELEMENT) {
                XmlNamed namedElement = path as XmlNamed;
                builder.element(namedElement.name.toString(), nest: () {
                  _addAttributes(path, builder);
                  _addNode(path, builder);
                });
              }
              else if (path.nodeType == XmlNodeType.TEXT && !path.text.trim().isEmpty) {
                builder.text(path.text.trim());
              }
            }
          }
        });
      });
    });

    return builder.build();
  }
}
