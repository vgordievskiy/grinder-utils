library grinder_utils.svg.sprite;
import 'dart:io';
import 'package:xml/xml.dart';
import 'package:html/parser.dart' as parser;
import 'package:html/dom.dart';
import 'dart:convert' show HtmlEscape;
import 'package:path/path.dart' as path;

class SvgSpriteResult {
  String svgSprite;
}

class SvgSprite {
  static const String xmlHeader = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';

  SvgSpriteResult combineSvgs (List<String> folders, List<String> excludeFolders, List<String> excludeFiles) {
    XmlBuilder builder = new XmlBuilder();
    Map<String, XmlNode> svgs = new Map<String, XmlNode>();
    SvgSpriteResult result = new SvgSpriteResult();

    for (String folder in folders) {
      Directory dir = new Directory(folder);

      if (!dir.existsSync()) {
        continue;
      }
      _collectSvgData(dir, svgs, excludeFolders, excludeFiles);
    }

    builder.element('svg', nest: () {
      builder.attribute('xmlns', 'http://www.w3.org/2000/svg');
      builder.attribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
      List<String> keys = svgs.keys.toList();
      keys.sort((a,b)=>_getId(a).compareTo(_getId(b)));

      for (String key in keys) {
          var node = svgs[key];
          builder.element('symbol', nest: () {
            builder.attribute('id', _getId(key));
            _addAttributes(svgs[key], builder,
              filterAttrubutes: ['xmlns', 'xmlns:xlink', 'id', 'transform']);
          
            _addNode(svgs[key], builder);
          });
      }
    });

    String svgSpriteXml = builder.build().toXmlString();
    result.svgSprite = xmlHeader + svgSpriteXml;
    return result;
  }

  String _getId (String filePath) {
    var stripSvg = path.withoutExtension(filePath);
    return stripSvg.replaceAll(' ', '-').replaceAll('\\', ' ').replaceAll('/', ' ').trim().replaceAll(' ', '--');
  }

  _addAttributes(XmlNode node, XmlBuilder  builder,
    { List<String> filterAttrubutes: const [], List<String> filterAttrubutesValues: const []})
  {
    for (XmlAttribute attr in node.attributes) {
      String attrName = attr.name.toString();
      if (!filterAttrubutes.any((String attr) => attrName.toLowerCase() == attr)
          && !filterAttrubutesValues.any((String val) => attr.value.contains(val))
         )
      {
        builder.attribute(attr.name.toString(), attr.value);
      }
    }
  }

  void _addNode(XmlNode parent, XmlBuilder  builder) {
    for (XmlNode node in parent.children) {
      if (node.nodeType == XmlNodeType.ELEMENT) {
        XmlNamed namedElement = node as XmlNamed;

        builder.element(namedElement.name.toString(), nest: () {
          node.attributes.forEach((XmlAttribute attr) {
            if (attr.name.local == 'id' && attr.value == 'key_stan')
            {
              builder.attribute('stroke-width', '1.7');
            }
            else if (attr.name.local == 'id' && attr.value == 'additional_line')
            {
              builder.attribute('stroke-width', '1.7');
            }
          });

          _addAttributes(node, builder, filterAttrubutesValues: ['translate']);
          _addNode(node, builder);
        });
      }  else if (node.nodeType == XmlNodeType.TEXT && !node.text.trim().isEmpty) {
        builder.text(node.text.trim());
      }
    }
  }

  XmlNode _getSvgNode(String svgText) {
    XmlDocument svgXml = parse(svgText);
    return svgXml.findElements("svg").first;
  }

  _collectSvgData(Directory dir, Map<String, XmlNode> svgs, List<String> excludeFolders, List<String> excludeFiles) {
    if (_checkPathInList(dir.path, excludeFolders)) {
      return;
    }

    dir.listSync(followLinks: false).forEach((FileSystemEntity package) {
      if (package is Directory) {
        _collectSvgData(package, svgs, excludeFolders, excludeFiles);
      } else if (path.extension(package.path).toLowerCase() == ".svg" && !_checkPathInList(package.path, excludeFiles)) {

        File f = package as File;
        svgs[new Uri.file(package.path).toFilePath()] = _getSvgNode(f.readAsStringSync());
        return;
      }
    });
  }

  bool _checkPathInList(String checkFile, List<String> files) {
    String filePath = new Uri.file(checkFile).toFilePath().toLowerCase();
    return files.any((String file)=>new Uri.file(file).toFilePath().toLowerCase() == filePath);
  }
}
