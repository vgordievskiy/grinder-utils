library grinder_utils.svg.config;
import 'dart:io';
import 'package:grinder_utils/grinder_utils.dart';
import 'package:yaml/yaml.dart' as yaml_lib;
import 'package:path/path.dart' as path;

class SvgSpriteConfig {
  String file;
  String filePath;
  List<String> paths;
  List<String> excludePaths;

  static SvgSpriteConfig read() {
    File f = new File("pubspec.yaml");
    String content = f.readAsStringSync();
    Map yaml = yaml_lib.loadYaml(content);
    var yamlCfg = (yaml['grinder_utils']);

    if (yamlCfg != null ) {
      var svgSpriteTask = yamlCfg['svg'];

      if (svgSpriteTask != null && svgSpriteTask['file'] != null && svgSpriteTask['paths'] != null) {
        return new SvgSpriteConfig()
          ..paths = svgSpriteTask['paths'] as List<String>
          ..excludePaths = svgSpriteTask['exclude'] as List<String>
          ..filePath = path.normalize(svgSpriteTask['file'])
          ..file = svgSpriteTask['file'];
      }
    }
    return null;
  }
}
