library grinder_utils.config;
import 'dart:io';
import 'package:path/path.dart' as path;

class AppConfig {
  String distDir = path.absolute('dist');
  String buildWebDir = path.absolute('build', 'web');
  String webDir = path.absolute('web');
}
